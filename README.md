EasyTester copyrigth by Miklos Boros

borosmiklos@gmail.com
miklos.boros@esss.se

This is an easy to use application to test EPICS IOC - PLC communication created by PLCFactory.

You can find detailed help in the EasyTester own help after lunching the application.
Help/View help...

Compatible PLCs are:
- Siemens S71500 family
- Beckhoff PLC with TwinCat3



